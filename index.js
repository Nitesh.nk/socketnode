var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 2530;

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
  // socket.on('chat message', function (msg) {
  //   io.emit('chat message', msg);
  // });

  socket.on('join', function (data) {
    console.log('===================join==============');
    console.log(data);
    console.log('====================================');
    socket.join(data.email);// Joining particular person
    // io.to('user@example.com').emit('message', { msg: 'hello world1.' }); //Server sending message to that particular person
    io.to(data.email).emit('message', { msg: 'hello world1.' }); //Server sending message to that particular person
  });
  socket.on('message', function (msg) {
    console.log('==============message===================');
    console.log(msg); // Server receiving message from that particular person
    console.log('====================================');
    io.to('user@example.com').emit('message', { msg: 'hello world2.' });//Server sending message to that particular person
  });
});

http.listen(port, function () {
  console.log('listening on *:' + port);
});
